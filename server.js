"use strict";

require("dotenv").config();

var express = require("express");
var mongo = require("mongodb");
var mongoose = require("mongoose");
var dns = require("dns");
var url = require("url");

var cors = require("cors");

var app = express();

// Basic Configuration
var port = process.env.PORT || 3000;

/** this project needs a db !! **/

mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(cors());

/** this project needs to parse POST bodies **/
// you should mount the body-parser here
var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/public", express.static(process.cwd() + "/public"));

app.get("/", function (req, res) {
  res.sendFile(process.cwd() + "/views/index.html");
});

// your first API endpoint...
app.get("/api/hello/:name", function (req, res) {
  res.json({ greeting: "hello " + req.params.name });
});

const Schema = mongoose.Schema;
const urlSchema = new Schema({
  original_url: String,
  short_url: String,
});
const urlModel = mongoose.model("url", urlSchema);

const inputDatabaseData = function () {};

app.post("/api/shorturl/new", function (req, res) {
  let value = 0;
  urlModel
    .find()
    .sort({ short_url: -1 })
    .limit(1)
    .exec((err, data) => {
      if (err) return console.error(err);
      value = Number(data[0].short_url);
      console.log("value", value);
      const newLink = new urlModel({
        original_url: req.body.url,
        short_url: value + 1,
      });
      dns.lookup(
        url.parse(newLink.original_url).hostname,
        (err, address, family) => {
          if (err) return console.error(err);
        }
      );
      newLink.save(function (err, data) {
        if (err) return console.error(err);
        else res.json(newLink);
      });
    });
});

app.get("/api/shorturl/:short_url", function (req, res) {
  const short_url = req.params.short_url;
  urlModel.findOne({ short_url: req.params.short_url }, function (err, data) {
    if (err) return console.error(err);
    res.redirect(data.original_url);
  });
});

app.listen(port, function () {
  console.log("Node.js is running on port: " + port);
});
